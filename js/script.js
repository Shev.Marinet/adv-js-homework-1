/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */

function Hamburger(size, stuffing) {
    let isSize = Object.getOwnPropertyNames(Hamburger.SIZES).some(function (item) {
        return item === size;
    });
    let isStuffing = Object.getOwnPropertyNames(Hamburger.STUFFINGS).some(function (item) {
        return item === stuffing;
    });
    if (isSize && isStuffing){
        this._size = size;
        this._staffing = stuffing;
        this._toppings = [];
    }
    try {
        if (!arguments.length) throw new HamburgerException ('no size given');
        else if (!isSize) throw new HamburgerException ('invalid size ' + size);
        else if (!isStuffing) throw new HamburgerException ('invalid stuffing ' + stuffing);
    }
    catch (e) {
        console.log(e.message);
    }

}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';
Hamburger.SIZES = {
    [Hamburger.SIZE_SMALL] : {
        price: 50,
        calories : 20
    },
    [Hamburger.SIZE_LARGE] : {
        price: 100,
        calories : 40
    }
};
Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD';
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO';
Hamburger.STUFFINGS = {
    [Hamburger.STUFFING_CHEESE] : {
        price: 10,
        calories : 20
    },
    [Hamburger.STUFFING_SALAD] : {
        price: 20,
        calories : 5
    },
    [Hamburger.STUFFING_POTATO] : {
        price: 15,
        calories : 10
    }
};
Hamburger.TOPPING_MAYO = 'TOPPING_MAYO';
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';
Hamburger.TOPPINGS ={
    [Hamburger.TOPPING_MAYO] : {
        price: 20,
        calories : 5
    },
    [Hamburger.TOPPING_SPICE] : {
        price: 15,
        calories : 0
    }
};
/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    if (!this._toppings.includes(topping)) {
        return this._toppings.push(topping);
    } else try {
        throw new HamburgerException ('duplicate topping ' + topping);
    }
    catch (e) {
        console.log(e.message);
    }
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    if (this._toppings.includes(topping)) {
        return this._toppings.splice(this._toppings.indexOf(topping),1);
    }
};
/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */

Hamburger.prototype.getToppings = function () {
    return this._toppings;
};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this._size;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this._staffing;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    return Hamburger.SIZES[this._size].price + Hamburger.STUFFINGS[this._staffing].price + this._toppings.reduce(function (sum, current) {
        return sum + Hamburger.TOPPINGS[current].price;
    }, 0);
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    return Hamburger.SIZES[this._size].calories + Hamburger.STUFFINGS[this._staffing].calories + this._toppings.reduce(function (sum, current) {
        return sum + Hamburger.TOPPINGS[current].calories;
    }, 0);
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException (message) {
    this.message = message;
    this.stack = (new Error()).stack;
}

HamburgerException.prototype = Object.create(Error.prototype);


// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1
console.log('Have ', hamburger.getStuffing(), ' stuffings'); // 1
console.log('Have ', hamburger.getToppings().join(', '), 'toppings'); // 1

// не передали обязательные параметры
var h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// HamburgerException: duplicate topping 'TOPPING_MAYO'
